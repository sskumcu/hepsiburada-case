from src.infrastructure.database.connection import HiveConnection, MongodbConnection


class Repository:

    @staticmethod
    def init():
        Repository.init_view_table()
        Repository.init_orders_table()

    @staticmethod
    def init_view_table():
        command = "create table if not exists  views(event string, messageid string, userid string," \
                  " properties struct<productid:string>, context struct<source:string>, ts bigint) " \
                  "row format delimited fields terminated by ',' stored as parquet location '/views'"
        conn = HiveConnection().establish_connection()
        cursor = conn.cursor()
        cursor.execute(command)

    @staticmethod
    def init_orders_table():
        command = "create table if not exists orders(event string, messageid string, " \
                  "userid string, orderid string, lineitems array<struct<productid:string, quantity:integer>>) " \
                  "row format delimited fields terminated by ',' stored as parquet location '/orders'"
        conn = HiveConnection().establish_connection()
        cursor = conn.cursor()
        cursor.execute(command)

    @staticmethod
    def select_latest_views(user_id):
        command = "select distinct properties.productid, ts, userid FROM views WHERE userid='{}' ORDER BY ts DESC LIMIT 10".\
            format(user_id)
        conn = HiveConnection().establish_connection()
        cursor = conn.cursor()
        cursor.execute(command)
        results = cursor.fetchall()
        return results

    @staticmethod
    def select_all_views(user_id):
        command = "select properties.productid, userid from views where userid='{}'".\
            format(user_id)
        conn = HiveConnection().establish_connection()
        cursor = conn.cursor()
        cursor.execute(command)
        results = cursor.fetchall()
        return results

    @staticmethod
    def select_most_bought_products(limit=10):
        command = "select COUNT(DISTINCT userid) AS product_count, itemsName.productid from orders " \
                  "LATERAL VIEW explode(lineitems) itemTable AS itemsName GROUP BY itemsName.productid " \
                  "ORDER BY product_count DESC LIMIT {}". \
            format(limit)
        conn = HiveConnection().establish_connection()
        cursor = conn.cursor()
        cursor.execute(command)
        results = cursor.fetchall()
        return results

    @staticmethod
    def select_most_common_products(product_ids):
        command = "select COUNT(DISTINCT userid) AS product_count, itemsName.productid from orders " \
                  "LATERAL VIEW explode(lineitems) itemTable AS itemsName  WHERE itemsName.productid IN ({}) " \
                  "GROUP BY itemsName.productid ORDER BY product_count DESC LIMIT 10".format(product_ids)
        conn = HiveConnection().establish_connection()
        cursor = conn.cursor()
        cursor.execute(command)
        results = cursor.fetchall()
        return results

    @staticmethod
    def get_category_id(product_id):
        doc = MongodbConnection().establish_connection("e-commerce", "product-category")
        return doc.find_one({"productid": product_id})['categoryid']

    @staticmethod
    def get_product_id(category_ids):
        product_ids = []
        doc = MongodbConnection().establish_connection("e-commerce", "product-category")
        for category_id in category_ids:
            for value in doc.find({"categoryid": category_id}):
                product_ids.append(value['productid'])
        return product_ids
