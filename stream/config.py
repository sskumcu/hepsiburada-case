schema = """
                    {
  "type": "record",
  "name": "View",
  "fields": [
    {
      "name": "event",
      "type": "string"
    },
    {
      "name": "messageid",
      "type": "string"
    },
    {
      "name": "userid",
      "type": "string"
    },
    {
      "name": "properties",
      "type": {
        "name": "properties",
        "type": "record",
        "fields": [
          {
            "name": "productid",
            "type": "string"
          }
        ]
      }
    },
    {
      "name": "context",
      "type": {
        "name": "context",
        "type": "record",
        "fields": [
          {
            "name": "source",
            "type": "string"
          }
        ]
      }
    }
  ]
}          

  """