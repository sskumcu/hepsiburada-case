#!/usr/bin/env bash

export PYSPARK_PYTHON=python3
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
SPARK_PATH=spark-2.4.5-bin-hadoop2.7
APP_PATH=stream.py
bash ${SPARK_PATH}/bin/spark-submit \
    --packages \
    org.apache.spark:spark-avro_2.11:2.4.5,org.apache.spark:spark-sql-kafka-0-10_2.11:2.4.5 \
    ${APP_PATH}


