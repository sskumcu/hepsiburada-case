# Assigment Case #

This project developed for Hepsiburada Recommendation Team Software/Data Engineer Assignment Case

Developed and tested on macOS Catalina Version 10.15.5, Docker Desktop 2.3.0.2

## Tech/framework used
* Hadoop
* MongoDB
* Hive
* Kafka
* Python / Flask
* Docker


## Project Structure ###

* [docker-hadoop-master](https://github.com/big-data-europe/docker-hadoop) - localhost:9870

* [docker-mongo-master](https://hub.docker.com/_/mongo) - localhost:8081

* [docker-hive-master](https://github.com/big-data-europe/docker-hive)

* [docker-kafka-master](https://hub.docker.com/r/confluentinc/cp-kafka/)

* offline - Application for pushing offline data; orders.json to HDFS and product-category-map.csv to MongoDB.

* simulation - This application pushes data (product-views.json) to Kafka per second, over 'views' topic. 

* src - Rest api developed with Flask. Creates and queries Hive tables and MongoDB, returns payload.

* stream - A spark stream app. Reads data from Kafka topic and writes to HDFS as parquet file.


![](hb-test-case-diagram.png) 

## Docker
### Images
I created five docker images, one base image for applications, others for specific requirements.

* py-app-base - Base image, installed ubuntu, java, python3
* simulation - application folder of simulation
* stream - application folder of stream
* bulk-data - application folder of offline
* rest-api - application folder of src

### Network
Created custom network named 'custom_network'. So containers can communicate each other.

## API Reference

GET /last_visit/{userid}

GET /best_seller/{userid}



Request URL

http://0.0.0.0/last_visit/user-41

Request Body
``` 
{
    "user_id": "user-41",
    "products": [
        "product-187",
        "product-878",
        "product-409",
        "product-239",
        "product-42",
        "product-149",
        "product-120"
    ],
    "type": "personalized"
}
``` 
Request URL

http://0.0.0.0/best_seller/user-41

Request Body

```
{
    "user_id": "user-41",
    "products": [
        "product-170",
        "product-83",
        "product-116",
        "product-34",
        "product-36",
        "product-172",
        "product-20",
        "product-120",
        "product-173",
        "product-136"
    ],
    "type": "personalized"
}
```

## How to use?
First, copy all data files (orders.json, product-category-map.csv, product-views.json) to data folder.

Then you can simply run run.sh file to setup environments and applications with docker.

```bash run.sh```  

## Personal Criticism
Holding view data on HDFS was not brilliant idea, at least not this way. Since I use parquet file format, ACID operations were not able to use, so I could not done Task 1.2.
Also, by the time, there will be bunch of small files on HDFS, so API response time will converge infinity. Elastic or Mongo might be used with foreach writer.
## Credits
Developer - Sarper Kumcu

Data Provider - Hepsiburada