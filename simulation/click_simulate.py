import io
import json
import time

import avro.schema
from avro.io import DatumWriter
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers='docker-kafka-master_kafka_1:9092')

TOPIC = "views"
schema_path = "view_schema.avsc"
schema = avro.schema.parse(open(schema_path).read())

with open('/data/product-views.json') as json_file:
    for line in json_file:
        data = json.loads(line)
        writer = DatumWriter(schema)
        bytes_writer = io.BytesIO()
        encoder = avro.io.BinaryEncoder(bytes_writer)
        writer.write(data, encoder)
        raw_bytes = bytes_writer.getvalue()
        producer.send(TOPIC, raw_bytes)
        time.sleep(1)
