from flask import Flask
from flask_restful import Resource, Api
from src.services.application_service import get_last_views, recommendation, init
app = Flask(__name__)
api = Api(app)


class History(Resource):
    def get(self, user_id):
        payload, code = get_last_views(user_id)
        return payload, code


class BestSeller(Resource):
    def get(self, user_id):
        payload, code = recommendation(user_id)
        return payload, code


api.add_resource(History, '/last_visit/<user_id>')
api.add_resource(BestSeller, '/best_seller/<user_id>')


if __name__ == '__main__':
    init()
    app.run(debug=True, host='0.0.0.0')