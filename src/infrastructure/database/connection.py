import pymongo
from pyhive import hive
from src.infrastructure.utils import config


class MongodbConnection:
    def establish_connection(self, database, collection):
        uri = config.MONGODB_URI.format(config.MONGODB_USERNAME, config.MONGODB_PASSWORD, config.MONGODB_HOST, config.MONGODB_PORT)
        return pymongo.MongoClient(uri)[database][collection]


class HiveConnection:
    def establish_connection(self):
        conn = hive.Connection(host=config.HIVE_HOST, port=config.HIVE_PORT)
        return conn
