from collections import Counter

from src.infrastructure.database.repositories import Repository


def get_last_views(user_id):
    last_views = []
    results = Repository.select_latest_views(user_id)
    if len(results) < 5:
        return {'user_id': user_id,
                'products': [],
                'type': "personalized"}, 200
    for result in results:
        last_views.append(result[0])
    return {'user_id': user_id,
            'products': last_views,
            'type': "personalized"}, 200


def recommendation(user_id):
    views = []
    results = Repository.select_all_views(user_id)
    if len(results) < 1:# If user have no view history
        return {'user_id': user_id,
                'products': [],
                'type': "non-personalized"}, 200
    for result in results:
        views.append(Repository.get_category_id(result[0]))
    results = get_most_common_products(views)
    products = []
    for result in results:
        products.append(result[1])
    if len(products) < 5:
        products = []
    return {'user_id': user_id,
            'products': products,
            'type': "personalized"}, 200


def get_most_common_products(views):
    most_common_products = [product for product, _ in Counter(views).most_common(3)]
    results = Repository.get_product_id(most_common_products)
    product_id_filter = ','.join(["'" + i + "'" for i in results])
    results = Repository.select_most_common_products(product_id_filter)
    return results


def init():
    Repository.init()
