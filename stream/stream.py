from pyspark import SparkConf
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.column import Column, _to_java_column
from pyspark.sql.functions import col, unix_timestamp
from pyspark.sql.streaming import DataStreamWriter
import config

class SparkStream:
    _session: SparkSession = None

    def __init__(self,
                 app_name: str,
                 master: str,
                 bootstrap: str,
                 topic: str,
                 checkpoint_location: str,
                 ):
        """
        :param app_name: App Name
        :param master: localhost[4]
        :param bootstrap: localhost:9092
        :param topic: topic
        :param checkpoint_location: file:// hdfs:// s3://
        :return:
        """
        self.app_name = app_name
        self.master = master
        self.bootstrap = bootstrap
        self.topic = topic
        self.schema = config.schema
        self.checkpoint_location = checkpoint_location

    def _spark_session(self) -> SparkSession:
        """
        Create new Spark session
        :return:
        """
        if self._session is not None:
            return self._session
        config = SparkConf()
        session = SparkSession \
            .builder \
            .appName(self.app_name) \
            .master(self.master) \
            .config(conf=config) \
            .getOrCreate()
        session.sparkContext.setLogLevel('WARN')
        self._session = session
        return session

    def data_frame(self) -> DataFrame:
        return self._spark_session() \
            .readStream \
            .format("kafka") \
            .option("kafka.bootstrap.servers", self.bootstrap) \
            .option("subscribe", self.topic) \
            .option("failOnDataLoss", "false") \
            .option("checkpointLocation", self.checkpoint_location) \
            .load() \
            .select(self.from_avro(col("value"), self.schema).alias("session"), "timestamp") \
            .select(col("session.*"), "timestamp") \
            .withColumn('ts', unix_timestamp(col("timestamp"), "yyyy-MM-dd HH:mm:ss.SSS")) \
            .drop("timestamp")

    def data_stream_writer(self) -> DataStreamWriter:
        """
        Creates a data simulation writer
        :return:
        """
        df = self.data_frame()
        return df.writeStream

    def write(self,
              path: str,
              partition: str = "date",
              output_mode: str = "append",
              write_format: str = "parquet",
              batch_time: str = "10 seconds"):
        """
        Writes simulation to a specified path with a specified format
        :param partition:
        :param batch_time:
        :param path:
        :param output_mode:
        :param write_format:
        :return:
        """
        self.data_stream_writer() \
            .outputMode(output_mode) \
            .format(write_format) \
            .option("checkpointLocation", self.checkpoint_location) \
            .option("path", path) \
            .start() \
            .awaitTermination()

    def read_parquet(self, path: str) -> DataFrame:
        """

        :param path:
        :return:
        """
        return self._spark_session().read.parquet(path)

    def from_avro(self, col_name: str, schema: str) -> Column:
        """

        :param col_name:
        :param schema:
        :return:
        """
        sc = self._session.sparkContext
        avro = sc._jvm.org.apache.spark.sql.avro
        f = getattr(getattr(avro, "package$"), "MODULE$").from_avro
        return Column(f(_to_java_column(col_name), schema))


    def run(self):
        self.write(path="hdfs://namenode:9000/views")


if __name__ == '__main__':
    SparkStream(app_name="consumer", master="local[*]", bootstrap="docker-kafka-master_kafka_1:9092", topic="views", checkpoint_location="hdfs://namenode:9000/views-cp").run()