from urllib.parse import quote_plus

import pymongo
from pyspark import SparkContext, SQLContext


def insert_product_category():
    """
    Insert product category file to MongoDB.
    :return:
    """
    import pandas as pd
    uri = "mongodb://%s:%s@%s" % (
        quote_plus("root"), quote_plus("example"), "docker-mongo-master_mongo_1:27017")
    client = pymongo.MongoClient(uri)
    database = client["e-commerce"]
    collection = database["product-category"]
    df = pd.read_csv("/data/product-category-map.csv")
    records_ = df.to_dict(orient='records')
    collection.insert_many(records_)


def insert_orders():
    """
    Insert orders bulk data to hadoop as parquet format.
    :return:
    """
    sc = SparkContext()
    sql_context = SQLContext(sc)
    df = sql_context.read.json("/data/orders.json")
    df.write.save('hdfs://namenode:9000/orders', format='parquet', mode='append')


if '__main__' == __name__:
    insert_product_category()
    insert_orders()

