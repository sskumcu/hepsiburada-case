
docker network create -d bridge custom_network
# run infrastructure
docker-compose -f docker-mongo-master/docker-compose.yml up -d
docker-compose -f docker-hadoop-master/docker-compose.yml up -d
docker-compose -f docker-kafka-master/docker-compose.yml up -d
docker-compose -f docker-hive-master/docker-compose.yml up -d


# BUILD CUSTOM IMAGES
# base image
docker build -t py-app-base .

# start producer simulation
docker build -t simulation -f simulation/Dockerfile .

# start stream app
docker build -t stream -f stream/Dockerfile .

# insert bulk data to hadoop and mongo
docker build -t bulk-data -f offline/Dockerfile .

# start rest api
docker build -t rest-api -f src/Dockerfile .


# RUN CUSTOM CONTAINERS
docker run -d --network=custom_network --name=simulation simulation
docker run -d --network=custom_network --name=stream stream
docker run -d --network=custom_network --name=bulk-data bulk-data
docker run -d -p 5000:5000 --network=custom_network --name=rest-api rest-api
